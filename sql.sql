create table student(sid int primary key auto_increment, stu_no char(20), name char(20))DEFAULT CHARSET=utf8;
create table exam(eid int primary key auto_increment, name char(20), perc float)DEFAULT CHARSET=utf8;
create table score(cid int primary key auto_increment, sid int, eid int, score float)DEFAULT CHARSET=utf8;
