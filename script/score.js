var ISLOGIN = null;
$(function(){

	var scoreData = null,
		examData = null;

	$.get('controller.php',function(data){ // get all data from backend
		scoreData = data.data.score;
		examData = data.data.exam;
		
		//append table head
		$.each( examData, function(i,value){ // run through every exam data
			var $nameDOM = $('.table-head .template.exam-name').clone(); // clone the exam name template;
			$nameDOM.text(value.name).removeClass('template'); //add text and remove template class so that it will be shown
			$nameDOM.insertBefore( $('.table-head .add-exam') );
		} );

		//append student
		$.each( scoreData, function(stu_no,stuInfo){ // the index of scoreData is stu_no
			var $stuDOM = $('.template.student-row').clone().removeClass('template');			
			$stuDOM.find('.stu_info').text( stu_no + ' ' + stuInfo.name );
			$stuDOM.data('sid',stuInfo.sid); // record sid so that we can get it

			//run for every exam 
			$.each( examData, function(i,examInfo){
				var eid = examInfo.eid;
				var stuScore = stuInfo[ eid ]; 
				// we need to use [] because eid is not a simply a name of attribute in stuInfo object,
				// it is a variable that contain the attribute we are trying to get from stuInfo.
				var $scoreDOM = $stuDOM.find('.template.score').clone();
				$scoreDOM.removeClass('template').text( stuScore );
				$scoreDOM.data('eid',examInfo.eid);
				$stuDOM.append( $scoreDOM ); // we don't have the add exam ui here, so new score would be the last one
			} );


			$stuDOM.insertBefore( $('.score-table .row.add-row') );
		} );
	});

	//add exam procedure
	//1.add new column with exam name replaced with an input tag
	//2.after entering the name, send the add exam request
	//3.bind the new exam id to the new column's cell
	$('.add-ui.add-exam').on('click',function(event){
		//append new table head
		var $nameDOM = $('.table-head .template.exam-name').clone(); // clone the exam name template;
		$nameDOM.empty().removeClass('template'); //add text and remove template class so that it will be shown
		$nameDOM.append( $('<input>').addClass('name').attr('placeholder','name') )
				.append( $('<input>').addClass('perc').attr('placeholder','percetage') )
		$nameDOM.insertBefore( $('.table-head .add-exam') );

	});
	$('.table-head').on('change','input',function(){
		var $this = $(this);
		var name = $this.parent().find('.name').val(),
			perc = $this.parent().find('.perc').val();
		if( name != "" && perc != "" ) {
			$.post('controller.php?job=addExam',{
				name: name,
				perc: perc
			},function(data){
				$('#notice').text( data.rtnMsg );
				if( data.rtnCode > 0 ) {
					$this.parent().empty().text(name);
					//append new column cell to content of the table
					$('.student-row:not(.template)').each(function(){
						var $scoreDOM = $(this).find('.template.score').clone();
						$scoreDOM.removeClass('template').empty();
						$scoreDOM.data('eid',data.data);
						$(this).append( $scoreDOM );
					});
				}
			});
		}
	});


	//add student procedure
	//1.add new row with name replaced with an input tag
	//2.after entering the name, send the add student request
	$('.add-ui.add-student').on('click',function(event){
		event.preventDefault();
		var $stuDOM = $('.template.student-row').clone().removeClass('template');			
			$stuDOM.find('.stu_info').empty()
				.append( $('<input>').addClass('stu_no').attr('placeholder','student no') )
				.append( $('<input>').addClass('name').attr('placeholder','name') );

		//run for every exam 
		$.each( examData, function(i,examInfo){
			var eid = examInfo.eid;
			var $scoreDOM = $stuDOM.find('.template.score').clone();
			$scoreDOM.removeClass('template');
			$scoreDOM.data('eid',examInfo.eid);
			$stuDOM.append( $scoreDOM ); // we don't have the add exam ui here, so new score would be the last one
		} );
		$stuDOM.insertBefore( $('.score-table .row.add-row') );
	});
	$('.score-table').on('change','.stu_info input',function(event){
		$stu = $(this).parent('.stu_info');
		var stu_no = $stu.find('.stu_no').val(),
			name = $stu.find('.name').val();
		if( stu_no != "" && name != "" ) {
			$.post('controller.php?job=addStudent',{
				stu_no : stu_no,
				name : name
			},function(data){
				$('#notice').text( data.rtnMsg );
				if( data.rtnCode > 0 ) {
					$stu.empty().text(stu_no + ' ' + name);
					$stu.parents('.student-row').data('sid',data.data);
				}
			})
		}
	});


	//update score procedure
	//1.click and change ui to input tag
	//2.when change, call api to update score, then change ui back to simple text
	$('.score-table').on('click','.score',function(event){
		if( $(this).find('input').length == 0 ) {//if there is no input tag, it means we need to put one
			var score = $(this).text();
			$(this).empty().append( $('<input>').val(score) );
			$(this).find('input').focus(); // auto focus the input box
		}
	}).on('change','.score input',function(event){
		//get essential information
		var $this = $(this);
		var newScore = $this.val(),
			eid = $this.parent('.score').data('eid'),
			sid = $this.parents('.student-row').data('sid');
		$.post('controller.php?job=updateScore',{
			sid: sid,
			eid: eid,
			score: newScore
		},function(data){
			if( data.rtnCode < 0 )
				$('#notice').text( data.rtnMsg );
			else { // update score successfully
				$this.parent('.score').empty().text( newScore );
			}
		});
	});

	$('.login-form').on('submit',function(event){ //Teacher login, trigger when the form is submitted
		event.preventDefault();
		if( $(this).find('input[type=password]').val() != "" ) { // check for not empty
			$.post('controller.php?job=login',{ // request for password verification
				password : $(this).find('input[type=password]').val()
			},function(data){
				$('#notice').text( data.rtnMsg ); // show return msg
				if( data.rtnCode > 0 ) { // if login successfully
					//hide login panel after login
					$('.login-container').hide();
					ISLOGIN = true;
				}
			});
		}
	});

	$('#notice').on('mouseover',function(){ // clear out notice messages after hover
		$(this).text('');
	});

	

});