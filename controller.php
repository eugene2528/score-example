<?php
session_start();
header('Content-Type: application/json');

$host = "localhost";
$user = "root";
$password = "eugene";
$teacher_password = "123";
$database = "score";
$dbConnection = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
$dbConnection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
$dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$rtn = ["rtnCode"=>"-9999","rtnMsg"=>""];

if( $_GET['job'] == "login" ) {
	if( isset($_POST['password']) ) {
		if( $_POST['password'] == $teacher_password ) {
			$_SESSION['score'] = "login";
			$rtn['rtnCode'] = "1000";
			$rtn['rtnMsg'] = "Login successfully.";
		}
		else {
			$rtn['rtnCode'] = "-1001";
			$rtn['rtnMsg'] = "Login failed.";
		}
	}
}
elseif( $_GET['job'] == "logout" ) {
	unset( $_SESSION['score'] );
	$rtn['rtnCode'] = "1002";
	$rtn['rtnMsg'] = "Logout successfully.";
}
elseif( $_GET['job'] == "addStudent" ) {
	if( $_SESSION['score'] != "login" ) {
		$rtn['rtnCode'] = "-9000";
		$rtn['rtnMsg'] = "Unauthorized";
	}
	else {
		if( isset( $_POST['name'] ) && isset( $_POST['stu_no'] ) ) {

			$request = $dbConnection -> prepare('select sid from student where stu_no=?;');
			$request->execute( array( $_POST['stu_no'] ) );
			$request -> setFetchMode(PDO::FETCH_ASSOC);
			$data = $request->fetchAll();

			if( count($data) == 0 ) {
				$request = $dbConnection -> prepare('insert into student(name,stu_no) values(?,?);');
				$request->execute( array( $_POST['name'], $_POST['stu_no'] ) );

				$rtn['data'] = $dbConnection->lastInsertId();
				$rtn['rtnCode'] = "2001";
				$rtn['rtnMsg'] = "Add student successfully.";
			}
			else {
				$rtn['rtnCode'] = "-2002";
				$rtn['rtnMsg'] = "Student number has existed.";
			}

		}
		else {
			$rtn['rtnCode'] = "-2001";
			$rtn['rtnMsg'] = "Missing parameters.";
		}
	}

}
elseif( $_GET['job'] == "addExam" ) {
	if( $_SESSION['score'] != "login" ) {
		$rtn['rtnCode'] = "-9000";
		$rtn['rtnMsg'] = "Unauthorized";
	}
	else {
		if( isset( $_POST['name'] ) && isset( $_POST['perc'] ) ) {

			$request = $dbConnection -> prepare('insert into exam(name,perc) values(?,?);');
			$request->execute( array( $_POST['name'], (float)$_POST['perc'] ) );

			$rtn['data'] = $dbConnection->lastInsertId();
			$rtn['rtnCode'] = "3001";
			$rtn['rtnMsg'] = "Add exam successfully.";

		}
		else {
			$rtn['rtnCode'] = "-3001";
			$rtn['rtnMsg'] = "Missing parameters.";
		}
	}
}
elseif( $_GET['job'] == "updateScore" ) {
	if( $_SESSION['score'] != "login" ) {
		$rtn['rtnCode'] = "-9000";
		$rtn['rtnMsg'] = "Unauthorized";
	}
	else {
		if( isset( $_POST['sid'] ) && isset( $_POST['eid'] ) && isset( $_POST['score'] ) ) {

			//............

			$request = $dbConnection -> prepare('select cid from score where sid=? and eid=?;');
			$request->execute( array( $_POST['sid'], $_POST['eid'] ) );
			$request -> setFetchMode(PDO::FETCH_ASSOC);
			$data = $request->fetchAll();

			if( count($data) == 0 ) { //score not existed
				$request = $dbConnection -> prepare('insert into score(sid,eid,score) values(?,?,?);');
				$request->execute( array( $_POST['sid'], $_POST['eid'], $_POST['score'] ) );

				$rtn['rtnCode'] = "3001";
				$rtn['rtnMsg'] = "Add score successfully.";
			}
			else { // score existed
				$request = $dbConnection -> prepare('update score set score=? where sid=? and eid=?;');
				$request->execute( array( $_POST['score'], $_POST['sid'], $_POST['eid'] ) );

				$rtn['rtnCode'] = "3002";
				$rtn['rtnMsg'] = "Update score successfully.";
			}

		}
		else {
			$rtn['rtnCode'] = "-3001";
			$rtn['rtnMsg'] = "Missing parameters.";
		}
	}
}
else {
	//get all data

	//get score data
	$request = $dbConnection -> prepare("select score.*,student.stu_no, student.name from score right join student on score.sid = student.sid;");
	$request->execute();
	$request -> setFetchMode(PDO::FETCH_ASSOC);
	$rawData = $request->fetchAll();

	$scoreData = array();
	for( $i=0; $i<count($rawData); $i++ ){
		if( !isset($scoreData[ $rawData[$i]['stu_no'] ]) ){
			$scoreData[ $rawData[$i]['stu_no'] ] = array();
			$scoreData[ $rawData[$i]['stu_no'] ]['sid'] = $rawData[$i]['sid'];
			$scoreData[ $rawData[$i]['stu_no'] ]['name'] = $rawData[$i]['name'];
		}
			
		$scoreData[ $rawData[$i]['stu_no'] ][ $rawData[$i]['eid'] ] = $rawData[$i]['score'];
	}

	//get exam data
	$request = $dbConnection -> prepare("select * from exam;");
	$request->execute();
	$request -> setFetchMode(PDO::FETCH_ASSOC);
	$examData = $request->fetchAll();

	$rtn['data'] = array( "score" => $scoreData, "exam" => $examData ); // combine two data together
	$rtn['rtnCode'] = "4000";
	$rtn['rtnMsg'] = "Get all data.";

}

echo json_encode( $rtn );


?>